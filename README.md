# README #



### What is this repository for? ###

* Recovery Selector is used to easily select the recovery partition on next restart on your Mac.
![Screenshot](https://twocanoes-app-resources.s3.us-east-1.amazonaws.com/recovery-selector/recovery.gif)

### Where do I download? ###

Just click on the [download link](https://bitbucket.org/twocanoes/recovery-selector/downloads/)

### How does it work? ###
It sets the follow nvram variables (and clears them):

key: internet-recovery-mode

values: RecoveryModeNetwork, RecoveryModeDisk, DiagsModeDisk, DiagsModeNetwork


key: recovery-boot-mode

value: unused

### Who do I talk to? ###

* Follow https://twitter.com/tperfitt on twitter