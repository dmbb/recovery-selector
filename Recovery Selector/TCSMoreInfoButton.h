//
//  TCSMoreInfoButton.h
//  Winclone
//
//  Created by Tim Perfitt on 5/16/18.
//  Copyright © 2018 Twocanoes Software Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TCSMoreInfoButton : NSButton

@end
