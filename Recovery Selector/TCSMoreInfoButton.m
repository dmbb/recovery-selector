//
//  TCSMoreInfoButton.m
//  Winclone
//
//  Created by Tim Perfitt on 5/16/18.
//  Copyright © 2018 Twocanoes Software Inc. All rights reserved.
//

#import "TCSMoreInfoButton.h"
#define SUPPORTURLS [[[NSBundle mainBundle] infoDictionary] objectForKey:@"Support URLs"]

@implementation TCSMoreInfoButton

-(void)awakeFromNib{
    
    NSColor *color = [NSColor linkColor];
    
    NSMutableAttributedString *colorTitle =
    [[NSMutableAttributedString alloc] initWithAttributedString:[self attributedTitle]];
    
    NSRange titleRange = NSMakeRange(0, [colorTitle length]);
    
    [colorTitle addAttribute:NSForegroundColorAttributeName
                       value:color
                       range:titleRange];
    
    [self setAttributedTitle:colorTitle];
    
    
}
- (void)mouseDown:(NSEvent *)event{
    
    
}
- (BOOL)acceptsFirstResponder{
    return NO;
}
- (void)mouseUp:(NSEvent *)event{
    
    NSURL *url=[NSURL URLWithString:SUPPORTURLS[self.identifier]];
    if (url) [[NSWorkspace sharedWorkspace] openURL:url];
}

@end
