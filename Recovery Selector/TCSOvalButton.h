//
//  TCSOvalButton.h
//  Winclone 6
//
//  Created by Tim Perfitt on 3/15/18.
//  Copyright © 2018 Twocanoes Software Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TCSOvalButton : NSButton
-(void)updateTitleColor;
@end
