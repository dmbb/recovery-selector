//
//  MDSPrivHelperToolController.m
//  MDS
//
//  Created by Timothy Perfitt on 10/6/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSPrivHelperToolController.h"
#import <Security/Security.h>
#import <ServiceManagement/ServiceManagement.h>
#import "RecoverySelectorHelperProtocol.h"
@interface TCSPrivHelperToolController(){
       AuthorizationRef        _authRef;
}
@property (strong) NSXPCConnection *privHelperToolConnection;
@end
@implementation TCSPrivHelperToolController
+ (instancetype)sharedHelper {
    static TCSPrivHelperToolController *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[[self class] alloc] initPrivate];
        if (![[self class] isToolInstalled]) {
            [sharedMyManager installTool];
        }

    });
    
    return sharedMyManager;
}
- (instancetype)initPrivate {
    return [super init];
}

- (instancetype)init {
    [[NSException exceptionWithName:@"Unimplemented" reason:@"Shared singleton must be used" userInfo:nil] raise];
    return nil;
}
+ (BOOL)isToolInstalled {
    NSFileManager *filemanager = [NSFileManager defaultManager];
    BOOL toolInstalled = [filemanager fileExistsAtPath:@"/Library/PrivilegedHelperTools/com.twocanoes.recoveryselectorhelper"];
    BOOL plistInstalled = [filemanager fileExistsAtPath:@"/Library/LaunchDaemons/com.twocanoes.recoveryselectorhelper.plist"];
    return toolInstalled && plistInstalled;
}
+ (BOOL)installTool{

    TCSPrivHelperToolController *controller=[[TCSPrivHelperToolController alloc] initPrivate];

    NSError *err;
    if([controller installToolError:&err]==NO){

        NSLog(@"%@",err.localizedDescription);
        return NO;
    }
    return YES;

}
- (void)connectToPrivHelperTool
{

    if (self.privHelperToolConnection == nil) {
        assert([NSThread isMainThread]);
        self.privHelperToolConnection = [[NSXPCConnection alloc] initWithMachServiceName:@"RecoverySelector" options:NSXPCConnectionPrivileged];
        self.privHelperToolConnection.remoteObjectInterface = [NSXPCInterface interfaceWithProtocol:@protocol(RecoverySelectorHelperProtocol)];

        [self.privHelperToolConnection.remoteObjectInterface setClasses:[NSSet setWithObjects:[TCSStartupDisk class],[NSMutableArray class],nil] forSelector:@selector(startupDisksWithCallback:) argumentIndex:0 ofReply:YES];

        [self.privHelperToolConnection.remoteObjectInterface setClasses:[NSSet setWithObjects:[TCSStartupDisk class],[NSMutableArray class],nil] forSelector:@selector(selectDisk:withCallback:) argumentIndex:0 ofReply:YES];

        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-retain-cycles"
        // We can ignore the retain cycle warning because a) the retain taken by the
        // invalidation handler block is released by us setting it to nil when the block
        // actually runs, and b) the retain taken by the block passed to -addOperationWithBlock:
        // will be released when that operation completes and the operation itself is deallocated
        // (notably self does not have a reference to the NSBlockOperation).
        self.privHelperToolConnection.invalidationHandler = ^{
            // If the connection gets invalidated then, on the main thread, nil out our
            // reference to it.  This ensures that we attempt to rebuild it the next time around.
            self.privHelperToolConnection.invalidationHandler = nil;
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                self.privHelperToolConnection = nil;
            }];
        };
#pragma clang diagnostic pop
        [self.privHelperToolConnection resume];

    }
}
- (id <RecoverySelectorHelperProtocol>)privHelperTool {
    [self connectToPrivHelperTool];
    id<RecoverySelectorHelperProtocol> proxyObject = [self.privHelperToolConnection remoteObjectProxyWithErrorHandler:^(NSError *error) {

        NSLog(@"%@",error.localizedDescription);
    }];

    return proxyObject;
}
- (void)installTool {

    NSError *error = nil;
    BOOL success = [self installToolError:&error];
    if (!success) {
        NSDictionary *info = @{NSUnderlyingErrorKey: error};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HelperNotification" object:self userInfo:info];
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self connectToPrivHelperTool];
    });

}
- (BOOL)installToolError:(NSError **)error {
    NSError *blessError;
    BOOL success = [self blessHelperWithLabel:@"com.twocanoes.recoveryselectorhelper" error:&blessError];
    if (!success && error != NULL) {
        *error = blessError;
    }
    return success;
}
- (void)installToolIfNecessary {
    NSInteger expectedVersion = 1;
    [[self privHelperTool] getVersionCallback:^(NSInteger version) {
        if (version != expectedVersion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self installTool];
                [self connectToPrivHelperTool];
            });
        }
    }];
}
- (void)getVersionCallback:(void (^)(NSInteger))callback{
    [self.privHelperTool getVersionCallback:^(NSInteger version) {
        callback(version);
    }];
}
-(void)selectDisk:(TCSStartupDisk *)diskInfo withCallback:(void (^)(NSInteger))callback {
    [self.privHelperTool selectDisk:diskInfo withCallback:^(NSInteger status) {
        callback(status);
    }];
}
-(void)unselectRecoveryWithCallback:(void (^)(NSInteger))callback {
    [self.privHelperTool unselectRecoveryWithCallback:^(NSInteger version) {
        callback(version);
}];
}

-(void)startupDisksWithCallback:(void (^)(NSArray *))done{
    [self.privHelperTool startupDisksWithCallback:^(NSArray *startupDisks) {
        done(startupDisks);
    }];

}

- (BOOL)blessHelperWithLabel:(NSString *)label error:(NSError **)errorPtr;
{
    OSStatus authStatus = AuthorizationCreate(NULL, kAuthorizationEmptyEnvironment, kAuthorizationFlagDefaults, &self->_authRef);
    if (authStatus != errAuthorizationSuccess) {
        /* AuthorizationCreate really shouldn't fail. */
        assert(NO);
        self->_authRef = NULL;
        return NO;
    }

    BOOL result = NO;
    NSError * error = nil;

    AuthorizationItem authItem        = { kSMRightBlessPrivilegedHelper, 0, NULL, 0 };
    AuthorizationRights authRights    = { 1, &authItem };
    AuthorizationFlags flags        =    kAuthorizationFlagDefaults                |
                                    kAuthorizationFlagInteractionAllowed    |
                                    kAuthorizationFlagPreAuthorize            |
                                    kAuthorizationFlagExtendRights;

    /* Obtain the right to install our privileged helper tool (kSMRightBlessPrivilegedHelper). */
    OSStatus status = AuthorizationCopyRights(self->_authRef, &authRights, kAuthorizationEmptyEnvironment, flags, NULL);
    if (status != errAuthorizationSuccess) {
        error = [NSError errorWithDomain:NSOSStatusErrorDomain code:status userInfo:nil];
    } else {
        CFErrorRef  cfError;

        /* This does all the work of verifying the helper tool against the application
         * and vice-versa. Once verification has passed, the embedded launchd.plist
         * is extracted and placed in /Library/LaunchDaemons and then loaded. The
         * executable is placed in /Library/PrivilegedHelperTools.
         */
        result = (BOOL) SMJobBless(kSMDomainSystemLaunchd, (__bridge CFStringRef)label, self->_authRef, &cfError);
        if (!result) {
            error = (__bridge NSError *)(cfError);
        }
    }
    if ( ! result && (errorPtr != NULL) ) {
        assert(error != nil);
        *errorPtr = error;
    }

    
    return result;
}

@end
