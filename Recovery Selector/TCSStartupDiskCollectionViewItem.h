//
//  TCSStartupDiskCollectionViewItem.h
//  Recovery Selector
//
//  Created by Timothy Perfitt on 1/25/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSStartupDiskCollectionViewItem : NSCollectionViewItem
@property (weak) IBOutlet NSTextField *titleTextView;
@property (weak) IBOutlet NSImageView *diskImageView;

@end

NS_ASSUME_NONNULL_END
