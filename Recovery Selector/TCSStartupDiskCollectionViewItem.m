//
//  TCSStartupDiskCollectionViewItem.m
//  Recovery Selector
//
//  Created by Timothy Perfitt on 1/25/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSStartupDiskCollectionViewItem.h"
#import "TCVolumeView.h"
@interface TCSStartupDiskCollectionViewItem ()

@end

@implementation TCSStartupDiskCollectionViewItem

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
- (void)setSelected:(BOOL)flag{

    [super setSelected:flag];
    [(TCVolumeView*)[self view] setSelected:flag];
    [(TCVolumeView*)[self view] setNeedsDisplay:YES];

}
@end
