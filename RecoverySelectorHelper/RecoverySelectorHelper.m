//
//  RecoverySelectorHelper.m
//  RecoverySelectorHelper
//
//  Created by Timothy Perfitt on 1/24/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "RecoverySelectorHelper.h"
#define LOGFILE "/Library/Logs/mdshelper.log"
#import <sys/mount.h>
#import "TCSConstants.h"
#import "TCSStartupDisk.h"
@interface RecoverySelectorHelper()
@property (atomic, strong, readwrite) NSXPCListener * listener;
@property (strong) NSString *installInstallMacOSScriptPath;
@property (assign) BOOL isCancelling;


@end
@implementation RecoverySelectorHelper
+ (NSString *)machServiceName { return @"RecoverySelector"; }
+ (NSString *)exposedProtocolName { return @"RecoverySelectorHelperProtocol"; }
void logLine(char *line){

    FILE *f_log;
    if((f_log=fopen(LOGFILE, "a"))==NULL){
        fprintf(stderr, "error opening log file");
        return;
    }

    size_t len=strnlen(line, 1024);
    fwrite(line,len, 1, f_log);
    fclose(f_log);

}


-(void)unselectRecoveryWithCallback:(void (^)(NSInteger))callback {

    NSTask *task=[NSTask launchedTaskWithLaunchPath:@"/usr/sbin/nvram" arguments:@[@"-d",@"recovery-boot-mode"]];

    [task waitUntilExit];
    callback(task.terminationStatus);

}
-(NSString *)currentStatupDisk:(id)sender{


    NSPipe *outputPipe=[NSPipe pipe];

    NSTask *systemSetupTask=[[NSTask alloc] init];
    systemSetupTask.launchPath=@"/usr/sbin/systemsetup";
    systemSetupTask.arguments=@[@"-getstartupdisk"];
    systemSetupTask.standardOutput=outputPipe;
    [systemSetupTask launch];
    [systemSetupTask waitUntilExit];
    NSData *dataToRead=[[outputPipe fileHandleForReading] readDataToEndOfFile];

    NSString *startupDiskOutput=[[NSString alloc] initWithData:dataToRead encoding:NSUTF8StringEncoding];
    return [startupDiskOutput stringByReplacingOccurrencesOfString:@"\n" withString:@""];




}
-(void)startupDisksWithCallback:(void (^)(NSArray <TCSStartupDisk * > *))done{

    NSMutableArray *startupDiskArray=[NSMutableArray array];

    NSPipe *outputPipe=[NSPipe pipe];

    NSTask *systemSetupTask=[[NSTask alloc] init];
    systemSetupTask.launchPath=@"/usr/sbin/systemsetup";
    systemSetupTask.arguments=@[@"-liststartupdisks"];
    systemSetupTask.standardOutput=outputPipe;
    [systemSetupTask launch];
    [systemSetupTask waitUntilExit];
    NSData *dataToRead=[[outputPipe fileHandleForReading] readDataToEndOfFile];

    NSString *startupDisksOutput=[[NSString alloc] initWithData:dataToRead encoding:NSUTF8StringEncoding];

    NSArray <NSString *> *outputArray=[startupDisksOutput componentsSeparatedByString:@"\n"];

    [outputArray enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        struct statfs fs;

        if(statfs(obj.UTF8String, &fs)==0){
            char *name=fs.f_mntonname;
            BOOL isCurrentStartupDisk=NO;
            if (name){
                NSString *currentStartupDisk=[self currentStatupDisk:self];
                if ([currentStartupDisk isEqualToString:obj]){
                    isCurrentStartupDisk=YES;
                }
                TCSStartupDisk *disk=[[TCSStartupDisk alloc] init];
                disk.name=[NSString stringWithUTF8String:name];
                disk.mountpoint=[NSString stringWithUTF8String:name];
                disk.bootloader=obj;
                disk.isCurrentStartupDisk=isCurrentStartupDisk;

                [startupDiskArray addObject:disk];
            }

        }

    }];

    done(startupDiskArray);

}

-(void)clearCurrentSettings:(id)sender{
    NSTask *task=[NSTask launchedTaskWithLaunchPath:@"/usr/sbin/nvram" arguments:@[@"-d",@"recovery-boot-mode"]];

    [task waitUntilExit];

    NSTask *internetRecoveryTask=[NSTask launchedTaskWithLaunchPath:@"/usr/sbin/nvram" arguments:@[@"-d",@"internet-recovery-mode"]];

    [internetRecoveryTask waitUntilExit];

}
-(void)selectDisk:(TCSStartupDisk *)inDisk withCallback:(void (^)(NSInteger))callback {

    [self clearCurrentSettings:self];

    if ((inDisk.type == RECOVERYTYPEOS) ||  (inDisk.type==RECOVERYTYPERECOVERY) ||
        (inDisk.type==RECOVERYTYPEINTERNETRECOVERY)){
        NSTask *task=[NSTask launchedTaskWithLaunchPath:@"/usr/sbin/systemsetup" arguments:@[@"-setstartupdisk",inDisk.bootloader]];
        [task waitUntilExit];
        if (inDisk.type == RECOVERYTYPEOS) {
            callback(task.terminationStatus);
            return;
        }
    }
    NSString *mode;
    switch (inDisk.type) {
        case RECOVERYTYPERECOVERY:
            mode=@"internet-recovery-mode=RecoveryModeDisk";

            break;

        case RECOVERYTYPEINTERNETRECOVERY:
            mode=@"internet-recovery-mode=RecoveryModeNetwork";
            break;

        case RECOVERYTYPEDIAGNOSTIC:
            mode=@"internet-recovery-mode=DiagsModeDisk";

            break;

        case RECOVERYTYPEINTERNETDIAGNOSTIC:
            mode=@"internet-recovery-mode=DiagsModeNetwork";

            break;


        default:
            break;
    }

    if (mode) {
        NSTask *otherTask=[NSTask launchedTaskWithLaunchPath:@"/usr/sbin/nvram" arguments:@[mode]];
          [otherTask waitUntilExit];

        [otherTask waitUntilExit];
        callback(otherTask.terminationStatus);

    }
   

}
- (void)getVersionCallback:(void (^)(NSInteger))callback {

    NSString *versionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    callback(versionString.integerValue);

}



-(BOOL)isCorrectlySignedClientWithPID:(int)pid{

    CFNumberRef value = NULL;
    CFDictionaryRef attributes = NULL;

    SecCodeRef code = NULL;
    OSStatus status;


    value = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &pid);
    if (!value) {

        logLine("Error creating number");
        return NO;

    }
    attributes = CFDictionaryCreate(kCFAllocatorDefault, (const void **)&kSecGuestAttributePid, (const void **)&value, 1, NULL, NULL);
    if (!attributes) {

        logLine("Error creating attributes");
        return NO;

    }
    status = SecCodeCopyGuestWithAttributes(NULL, attributes, kSecCSDefaultFlags, &code);
    if (status!=0) {

        logLine("Error creating SecCodeCopyGuestWithAttributes");
        return NO;

    }

    NSString *entitlement = @"anchor apple generic and certificate leaf[subject.OU] = UXP6YEHSPW";
    SecRequirementRef requirement = NULL;
    CFErrorRef errors = NULL;
    status = SecRequirementCreateWithStringAndErrors((__bridge CFStringRef)entitlement, kSecCSDefaultFlags, &errors, &requirement);
    if (status!=0) {

        logLine("Error creating SecRequirementCreateWithStringAndErrors");
        return NO;

    }
    status = SecCodeCheckValidity(code, kSecCSDefaultFlags, requirement);
    if (status!=0) {

        logLine("Request from invalid client. Refusing");
        return NO;

    }

    return YES;

}
-(BOOL)isScriptValid:(NSString *)scriptPath{

    BOOL isValid=NO;
    NSFileManager *fm=[NSFileManager defaultManager];
    if ([fm fileExistsAtPath:scriptPath]){

        NSDictionary *dict=[fm attributesOfItemAtPath:scriptPath error:nil];

        if (dict && [dict objectForKey: NSFilePosixPermissions] && [[dict objectForKey: NSFilePosixPermissions] intValue]==0755 && [dict objectForKey:NSFileOwnerAccountID] && [[dict objectForKey:NSFileOwnerAccountID] intValue]==0){
            isValid=YES;
        }
    }

    return isValid;
}
- (BOOL)listener:(NSXPCListener *)listener shouldAcceptNewConnection:(NSXPCConnection *)newConnection
// Called by our XPC listener when a new connection comes in.  We configure the connection
// with our protocol and ourselves as the main object.
{

    if([self isCorrectlySignedClientWithPID:newConnection.processIdentifier]==NO){
        return NO;
    }

    assert(listener == self.listener);
#pragma unused(listener)
    assert(newConnection != nil);
    NSXPCInterface *interface = [NSXPCInterface interfaceWithProtocol:NSProtocolFromString([[self class] exposedProtocolName])];
    newConnection.exportedInterface = interface;
    newConnection.exportedObject = self;
    [newConnection.exportedInterface setClasses:[NSSet setWithObjects:[TCSStartupDisk class],nil] forSelector:@selector(startupDisksWithCallback:) argumentIndex:0 ofReply:NO];

    [newConnection.exportedInterface setClasses:[NSSet setWithObjects:[TCSStartupDisk class],nil] forSelector:@selector(selectDisk:withCallback:) argumentIndex:0 ofReply:NO];


    [newConnection resume];


    return YES;
}
- (void)runXPCService
{

    // Tell the XPC listener to start processing requests.
        self.listener = [[NSXPCListener alloc] initWithMachServiceName:[[self class] machServiceName]];
        self.listener.delegate = self;
        [self.listener resume];

//     Run the run loop forever.

        [[NSRunLoop currentRunLoop] run];
}




@end
