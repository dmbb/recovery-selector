//
//  main.m
//  winclone_disk_helper
//
//  Created by Steve Brokaw on 2/9/17.
//  Copyright © 2017 Twocanoes Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecoverySelectorHelper.h"


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSLog(@"Starting Priv Helper Tool");
        RecoverySelectorHelper *helperTool = [[RecoverySelectorHelper alloc] init];
        [helperTool runXPCService]; // Never returns ...
    }
    NSLog(@" Helper Tool Launch Failure");
    return EXIT_FAILURE;        // ... so this should never be hit.
}
