//
//  Includes.h
//  Certificate Request
//
//  Created by Tim Perfitt on 3/4/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#ifndef Includes_h
#define Includes_h


#endif /* Includes_h */

#define RECOVERYTYPEOS 0
#define RECOVERYTYPERECOVERY 1
#define RECOVERYTYPEINTERNETRECOVERY 2
#define RECOVERYTYPEDIAGNOSTIC 3
#define RECOVERYTYPEINTERNETDIAGNOSTIC 4
